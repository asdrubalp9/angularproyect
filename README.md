# BiceTest

Este proyecto fue realizado con [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

Cabe resaltar que para el momento de la elaboración de este proyecto, mis conocimientos de angular eran practicamente nulos y solo le podia dedicar una hora diaria a este proyecto ya que tengo un trabajo y algunas otras responsabilidades parcelisticas (si, en este momento vivo en una parcela, y a pesar que no hay chivos, igual toca abrir los riegos y otras tareas, pero ando trabajando en la automatizacion y monitoreo de todo esto.... es un cuento largo, si eres digno te lo contare).

para cumplir con lo siguiente:


# Test de programación - Banco Bice Lab

El objetivo del test de programación es que demuestres tus capacidades y especialización.
Para lograrlo, te pedimos crear un nuevo proyecto público en tu repositorio GIT personal de preferencia (Github, Gitlab, Bitbucket, etc.) y utilizando el lenguaje de programación que más te apasione junto con toda tu creatividad, esperamos que desarrolles una aplicación Web (Front, Back o Fullstack) que nos deje con la boca abierta.

# Restricciones:

1. Se debe conectar por lo menos a un servicio de https://www.indecon.online/ para obtener
uno o varios indicadores económicos.
2. La aplicación debe tener por lo menos 1 test unitario de la funcionalidad que consideres
más crítica.
3. El README del proyecto debe tener todas las instrucciones de instalación y ejecución
tanto de la aplicación como del test.
4. Se tienen 7 días desde el momento en que se recibe este documento para enviar el link
del repositorio público del proyecto al email: `omitido por protección` con el subject
“Test programación Lab Banco Bice + <tu nombre>”.
Criterios de evaluación:
1. Completitud. La aplicación debe cumplir con todas las restricciones descritas y
funcionar correctamente.
2. Calidad. Se revisará la aplicación de buenas prácticas de ingeniería de software.
3. Cantidad. Más es menos, no nos interesa que pases 1 semana desarrollando sin parar
ni tampoco queremos demorarnos 1 semana en revisar, solo bastan unas cuantas horas
de desarrollo para demostrar lo que eres capaz de hacer.
4. Ciberseguridad. Se le aplicará revisión de código automático para detectar
vulnerabilidades mínimas de seguridad.
5. Creatividad. No hay restricciones y por eso mismo, aprovecha de no quedarte con lo
básico. Deja que tu código nos diga cuál es tu nivel.
A considerar:
1. A pesar de no haber restricciones con el uso de tecnologías, recomendamos Angular y
NodeJS.


## Como correr este repo

Primero clonas este repo, luego corres `npm install `, mientras esperas te haces un cafe, y cuando todo este listo, escribes `ng serve` para iniciar el DEV server. finalmente abres chrome e ingresas a `http://localhost:4200/`. 

## Para ver los test

En el command prompt, escribes `ng test` para ejecutar los  unit tests via [Karma](https://karma-runner.github.io).