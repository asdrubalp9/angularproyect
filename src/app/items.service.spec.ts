import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { ItemsService } from './items.service';
import { environment } from './../environments/environment';

describe('ItemsService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);

    service = TestBed.inject(ItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test para el api de indicadores', () => {
    it('Test para ver si viene la data correcta del servidor', () => {
      // arrange - preparar
      const expectData = [
        {
          date: 1606953600,
          key: 'ipc',
          name: 'Indice de Precios al Consumidor (Var. c/r al período anterior)',
          unit: 'porcentual',
          value: null
        },
      ];

      let dataError;
      let dataResponse;

      // act - actuacion
      service.getIndicadorByDate( '03-12-2020', 'ipc').subscribe(
        (response) => {
          dataResponse = response;
        },
        (err) => {
          dataError = err;
        }
      );

      const req = httpTestingController.expectOne(
        `${environment.apiBaseUrl}date/ipc/03-12-2020`
      );
      req.flush(expectData);

      // assert
      expect(dataResponse.length).toEqual(1);
      expect(req.request.method).toEqual('GET');
      expect(dataError).toBeUndefined();
    });
  });

});
