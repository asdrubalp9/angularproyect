import { Component, OnInit, Input, Output, EventEmitter, NgModule } from '@angular/core';
import { ItemsService } from './../items.service';
import { Indicador } from './../models/indicador.model';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private itemsService: ItemsService
  ) { }
  
  ///////////////////////////////////////
  modalTitle = '';
  showChart: boolean = false;
  lineChartData: ChartDataSets[] = [
    {
      data: [] ,
      label: ''
    },
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';



  ////////////////////////////////////////

  indicadores = [];
  fecha = [];


  @Output() itemClicked: EventEmitter<any>;

  searchByDate(indicador: string, fecha: string, index: number){

    if( !fecha[index] ){
      alert('Debe ingresar una fecha');
      return false;
    }

    const dateElement = fecha[index].split('-');
    const strDate = `${dateElement[2]}-${dateElement[1]}-${dateElement[0]}`;
    this.itemsService.getIndicadorByDate( strDate , indicador).subscribe(items => {
      console.log('byDate', items, items['value']);
      const msg = items['value'] ? `Para la fecha ${strDate}, el precio del ${indicador} era de ${items['value']}` : `No tenemos el valor del ${indicador} Para la fecha ${strDate}`;

      alert( msg )
    });

  }
  closeModal() {
    this.showChart = false;

  }

  getChart( type: string, name: string ){

    this.lineChartData[0].label = name;
    this.itemsService.getIndicadorData(type).subscribe(items => {
      this.showChart = true;
      this.modalTitle = type;
      console.log('byData', items);

      this.lineChartData[0].data = [];
      
        Object.values(items['values']).map( a => {
          
          (this.lineChartData[0].data as number[]).push( a as number );
          //(this.lineChartData[0].data as number[]).push( a as number );
        });
      

      Object.keys(items).map( a => {

        this.lineChartLabels = [];

        Object.keys(items[a]).forEach(element => {
          const fecha = new Date(   parseInt(element,10)  * 1000  );
          const label = `${fecha.getDate()}-${fecha.getMonth()}-${fecha.getFullYear()}`;
          this.lineChartLabels.push( label );
        });

      });
    });

  }

  ngOnInit(): void {

    this.itemsService.getUrlFromApi('/last').subscribe(items => {

      Object.keys(items).map( a => {

        this.indicadores.push( items[a] );
      });

      console.log( this.indicadores);
    });

  }

}
