import {Component, OnInit } from '@angular/core';
import { ItemsService } from './../../../items.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private itemsService: ItemsService
  ) { }

  ngOnInit(): void {
    this.itemsService.getBaseUrl().subscribe(items => (
      console.log('items', items)
    ));
  }

}
