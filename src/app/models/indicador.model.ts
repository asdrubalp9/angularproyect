export interface Indicador {
    fecha: string;
    nombre: string;
    title: string;
    unit: string;
    valor: number;
}