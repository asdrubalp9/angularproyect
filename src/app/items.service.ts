import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(
    private http: HttpClient
  ) { }
  baseUrl = environment.apiBaseUrl;

  getBaseUrl(){
    return this.http.get(this.baseUrl);
  }

  checkIfServerActive(){
    return this.http.get(this.baseUrl);
  }

  getUrlFromApi(url: string){
    return this.http.get(this.baseUrl + url);
  }

  getIndicadorByDate( date:string, type:string){
    let url = `${this.baseUrl}date/${type}/${date}`;
    console.log(url)
    return this.http.get(url);
  }

  getIndicadorData(type: string){
    return this.http.get(this.baseUrl + '/values/' + type);
  }

}
